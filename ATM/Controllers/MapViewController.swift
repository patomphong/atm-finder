import UIKit
import GoogleMaps
import GooglePlaces
import GoogleMapsCore
import GoogleMobileAds
import ReachabilitySwift

class MapViewController: UIViewController, GADInterstitialDelegate {
    
    var interstitial: GADInterstitial!
  
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var mapCenterPinImage: UIImageView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var pinImageVerticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonRadius: UIBarButtonItem!
    
    var searchedTypes = ["atm", "bank"]
    let locationManager = CLLocationManager()
    let dataProvider = GoogleDataProvider()
    var searchRadius: Double = 1000.0
    var titleButtonRadius: String = "1 km"
    
    let reachability = Reachability()!
    var isReLoadLocation = false
    var isLocationMove = false
    
    lazy var titleView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 60))
        view.backgroundColor = UIColor.clear
        
        return view
    }()
    
    private lazy var informLabel: UILabel = {
        var label = UILabel(frame: CGRect(x: 0, y: self.titleView.frame.maxY - 34, width: self.titleView.frame.width, height: 34))
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.backgroundColor = UIColor.black
        label.isHidden = false
        self.view.insertSubview(label, belowSubview: self.titleView)

        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()

        mapView.delegate = self

        self.title = "ATM Finder"
        buttonRadius.title = titleButtonRadius

        setupReachability()
        startNotifier()
        
        isReLoadLocation = true
        
        //Create And Load Admob
        interstitial = createAndLoadInterstitial()
    }
    
    func createAndLoadInterstitial() -> GADInterstitial {
        var interstitial = GADInterstitial(adUnitID: "ca-app-pub-7330863646599112/4106901017")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "Types Segue" {
      let navigationController = segue.destination as! UINavigationController
      let controller = navigationController.topViewController as! TypesTableViewController
      controller.selectedTypes = searchedTypes
      controller.delegate = self
    }
    if segue.identifier == "Radius Segue" {
        let navigationController = segue.destination as! UINavigationController
        let controller = navigationController.topViewController as! RadiusTableViewController
        controller.selectedRadius = searchRadius
        controller.delegate = self
    }
    }
  
    func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()

        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
          self.addressLabel.unlock()
          if let address = response?.firstResult() {
            
            //let lines = address.lines as! [String]
            let lines = address.lines!
            self.addressLabel.text = lines.joined(separator: "\n")
            
            let labelHeight = self.addressLabel.intrinsicContentSize.height
            self.mapView.padding = UIEdgeInsets(top: self.topLayoutGuide.length, left: 0, bottom: labelHeight, right: 0)
            
            UIView.animate(withDuration: 0.25, animations: {
              self.pinImageVerticalConstraint.constant = ((labelHeight - self.topLayoutGuide.length) * 0.5)
              self.view.layoutIfNeeded()
            }) 
          }
        }
    }
  
    func fetchNearbyPlaces(_ coordinate: CLLocationCoordinate2D) {
        isLocationMove = false
        mapView.clear()
        dataProvider.fetchPlacesNearCoordinate(coordinate, radius:searchRadius, types: searchedTypes, pagetoken: "") { places in
         
            var i = 1
            for place: GooglePlace in places {
            print("i = \(i)")
            let marker = PlaceMarker(place: place)
            marker.map = self.mapView
                i += 1
          }
        
//        if self.dataProvider.pagetokenSecond != "" {
//            self.dataProvider.fetchPlacesNearCoordinateNextPage(coordinate, radius:self.searchRadius, types: self.searchedTypes, pagetoken: self.dataProvider.pagetokenSecond) { places in
//                for place: GooglePlace in places {
//                    let marker = PlaceMarker(place: place)
//                    marker.map = self.mapView
//                }
//            }
//        }
        }
    }
    
    @IBAction func refreshPlaces(_ sender: AnyObject) {

        print("press fetch")
        
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
        }

        
        checkLocationPermission()
        fetchNearbyPlaces(mapView.camera.target)
    }
    
    func setupReachability() {
        reachability.whenReachable = { reachability in
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            DispatchQueue.main.async {
                print("Have Internet Connection")
                self.hideInformationMessage()
                
                if self.isReLoadLocation && self.isLocationMove {
                    self.fetchNearbyPlaces(self.mapView.camera.target)
                }

            }
        }
        reachability.whenUnreachable = { reachability in
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            DispatchQueue.main.async {
                print("No Internet Connection")
                self.showInformationMessage(text: "No Internet Connection", textColor: UIColor.white, backgroundColor: UIColor.black)
            }
        }
        
    }
    
    func startNotifier() {
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    func checkLocationPermission() {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                print("No access")
                
                let alert = UIAlertController(
                    title: "Allow ATM Finder to access your location while you use the app?",
                    message: "By accessing your lcation, this app can find ATM.",
                    preferredStyle: UIAlertControllerStyle.alert
                )
                
                alert.addAction(UIAlertAction(title: "Don't Allow", style: .default, handler: nil))
                alert.addAction(UIAlertAction(title: "Allow", style: .cancel, handler: { (alert) -> Void in
                    UIApplication.shared.openURL(NSURL(string: UIApplicationOpenSettingsURLString)! as URL)
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        } else {
            print("Location services are not enabled")
        }
    }
    
    func showInformationMessage(text: String?, textColor: UIColor!, backgroundColor: UIColor?) {
        self.informLabel.text = text
        self.informLabel.textColor = textColor
        self.informLabel.backgroundColor = backgroundColor
        self.informLabel.alpha = 0.75
        self.informLabel.isHidden = false
        let maxY = self.titleView.frame.maxY

        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.informLabel.frame.origin.y = maxY
        })
    }
    
    func hideInformationMessage() {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.informLabel.frame.origin.y = self.titleView.frame.maxY - 34
        }) { (finished) -> Void in
            self.informLabel.isHidden = true
        }
    }
  
}

// MARK: - TypesTableViewControllerDelegate
extension MapViewController: TypesTableViewControllerDelegate {
  func typesController(_ controller: TypesTableViewController, didSelectTypes types: [String]) {
    print("didSelectTypes")
    searchedTypes = controller.selectedTypes.sorted()
    dismiss(animated: true, completion: nil)
    fetchNearbyPlaces(mapView.camera.target)
  }
}

// MARK: - RadiusTableViewControllerDelegate
extension MapViewController: RadiusTableViewControllerDelegate {
    func radiusController(_ controller: RadiusTableViewController, didSelectRadius radius: Double) {
        print("didSelectRadius")
        
        guard let radius = controller.selectedRadius else {
            return
        }
        
        searchRadius = radius
        dismiss(animated: true, completion: nil)
        fetchNearbyPlaces(mapView.camera.target)
        print("controller.selectedRadius = \(radius)")
        
        switch radius {
        case 100.0:
            titleButtonRadius = "100 m"
        case 500.0:
            titleButtonRadius = "500 m"
        case 1000.0:
            titleButtonRadius = "1 km"
        case 5000.0:
            titleButtonRadius = "5 km"
        case 10000.0:
            titleButtonRadius = "10 km"
        default:
            titleButtonRadius = "1 km"
        }
        
        buttonRadius.title = titleButtonRadius
        
    }
}

// MARK: - CLLocationManagerDelegate
extension MapViewController: CLLocationManagerDelegate {
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    if status == .authorizedWhenInUse {
      locationManager.startUpdatingLocation()
      mapView.isMyLocationEnabled = true
      mapView.settings.myLocationButton = true
    }
  }
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    print("didUpdateLocations")
    if let location = locations.first {
      mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
      locationManager.stopUpdatingLocation()
      fetchNearbyPlaces(location.coordinate)
    }
  }
}

// MARK: - GMSMapViewDelegate
extension MapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocodeCoordinate(position.target)
    }

    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {

        print("mapView willMove")

        isLocationMove = true

        addressLabel.lock()

        if (gesture) {
          mapCenterPinImage.fadeIn(0.25)
          mapView.selectedMarker = nil
          //fetchNearbyPlaces(mapView.camera.target)
        }
    }
  
    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
        let placeMarker = marker as! PlaceMarker

        if let infoView = UIView.viewFromNibName("MarkerInfoView") as? MarkerInfoView {
          infoView.nameLabel.text = placeMarker.place.name
          infoView.addressLabel.text = placeMarker.place.address
          
          if let photo = placeMarker.place.photo {
            infoView.placePhoto.image = photo
          } else {
            infoView.placePhoto.image = UIImage(named: "atm")
          }
          
          return infoView
        } else {
          return nil
        }
    }
  
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
    mapCenterPinImage.fadeOut(0.25)
    return false
    }

    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
    mapCenterPinImage.fadeIn(0.25)
    mapView.selectedMarker = nil
    return false
    }
}


