//
//  RadiusTableViewController.swift
//  ATM
//
//  Created by Patomphong Wongkalasin on 1/24/2560 BE.
//  Copyright © 2560 ReedUs Tech. All rights reserved.
//

import UIKit
import Firebase

protocol RadiusTableViewControllerDelegate: class {
    func radiusController(_ controller: RadiusTableViewController, didSelectRadius radius: Double)
}

class RadiusTableViewController: UITableViewController {
    
    let possibleRadiusDictionary = [100.0:"100 m", 500.0:"500 m", 1000.0:"1 km", 2000.0:"2 km", 3000.0:"3 km", 4000.0:"4 km", 5000.0:"5 km", 10000.0:"10 km"]
    var selectedRadius: Double!
    weak var delegate: RadiusTableViewControllerDelegate!
    var sortedKeys: [Double] {
        return possibleRadiusDictionary.keys.sorted()
    }
    
    // MARK: - Actions
    @IBAction func donePressed(_ sender: AnyObject) {
        print("Press Done Button")
        
        guard let radius = selectedRadius else {
            return
        }
        
        delegate?.radiusController(self, didSelectRadius: radius)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return possibleRadiusDictionary.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RadiusCell", for: indexPath)

        let key = sortedKeys[(indexPath as NSIndexPath).row]
        let radius = possibleRadiusDictionary[key]!
        cell.textLabel?.text = radius
        cell.accessoryType = getRadiusTitleWithRadius(radius: selectedRadius!) == radius ? .checkmark : .none
        
      //  cell.accessoryType = (selectedRadius!).contains(key) ? .checkmark : .none

        return cell
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let key = sortedKeys[(indexPath as NSIndexPath).row]
//        if (selectedRadius!).contains(key) {
//            //selectedRadius = selectedRadius.filter({$0 != key})
//            selectedRadius = key
//        } else {
//            selectedRadius.append(key)
//        }
        
        selectedRadius = key
        
        guard let radius = selectedRadius else {
            return
        }
        
        selectedRadius = radius
        
        print("Selected Radius : \(radius)")
        
        logEventSelectRadius(radius: "\(radius)")
        
        tableView.reloadData()
        
        delegate?.radiusController(self, didSelectRadius: radius)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func getRadiusTitleWithRadius(radius: Double) -> String {
        switch radius {
        case 100.0:
            return "100 m"
        case 500.0:
            return "500 m"
        case 1000.0:
            return "1 km"
        case 5000.0:
            return "5 km"
        case 10000.0:
            return "10 km"
        default:
            return "1 km"
        }
    }
    
    func logEventSelectRadius(radius:String) {
        
        Analytics.setUserProperty(radius, forName: "radius")
        
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: radius as NSObject,
            AnalyticsParameterContentType: "radius" as NSObject
            ])
    }
}
