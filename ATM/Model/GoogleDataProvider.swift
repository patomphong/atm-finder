import UIKit
import Foundation
import CoreLocation
import SwiftyJSON
import Alamofire

class GoogleDataProvider {
  var photoCache = [String:UIImage]()
  var placesTask: URLSessionDataTask?
  var session: URLSession {
    return URLSession.shared
  }
    
    var placesArray = [GooglePlace]()
    
  let googlePlacesApiKey = "AIzaSyBytPuLLDzUWEY3oLpy3vi5b0mezpmd5mw"
    var pagetokenSecond: String = ""
  
    func fetchPlacesNearCoordinate(_ coordinate: CLLocationCoordinate2D, radius: Double, types:[String], pagetoken: String, completion: @escaping (([GooglePlace]) -> Void)) -> ()
  {
//    var urlString = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(coordinate.latitude),\(coordinate.longitude)&radius=\(radius)&rankby=prominence&sensor=true"
    var urlString = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(coordinate.latitude),\(coordinate.longitude)&radius=\(radius)"
    let typesString = types.count > 0 ? types.joined(separator: "|") : "atm"
    urlString += "&types=" + typesString
    urlString += "&key=" + googlePlacesApiKey
    
    self.placesArray = []
    
//    if pagetoken != "" {
//        urlString += "&pagetoken=" + pagetoken
//    }else{
//        self.placesArray = []
//    }
    
    
    print("urlString = " + urlString)
    
    urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
    
    if let task = placesTask , task.taskIdentifier > 0 && task.state == .running {
      task.cancel()
    }

    UIApplication.shared.isNetworkActivityIndicatorVisible = true
    placesTask = session.dataTask(with: URL(string: urlString)!, completionHandler: {data, response, error in
      UIApplication.shared.isNetworkActivityIndicatorVisible = false
   //   self.placesArray = []
      if let aData = data {
        let json = JSON(data:aData, options:JSONSerialization.ReadingOptions.mutableContainers, error:nil)
        if let results = json["results"].arrayObject as? [[String : AnyObject]] {
          for rawPlace in results {
            let place = GooglePlace(dictionary: rawPlace, acceptedTypes: types)
            self.placesArray.append(place)
            
            var imgName = ""
            
            switch place.placeType{
                
                case "atm":
                    imgName = "atm"
            
                case "bank":
                    imgName = "bank"
                
                default:
                    imgName = "atm"
            }
            
            place.photo = UIImage(named: imgName)
            
            //set real photo place
//            if let reference = place.photoReference {
//              self.fetchPhotoFromReference(reference) { image in
//                place.photo = image
//              }
//                print("photoReference = \(reference)")
//            }
          }
            
            print("place size = \(self.placesArray.count)")
        }
        
//        if let next_page_token = json["next_page_token"].string {
//            self.pagetokenSecond = next_page_token
//            print("next_page_token = \(next_page_token)")
//            
//           // self.placesArray += self.fetchNextPage(coordinate, radius: radius, types: types, pagetoken: next_page_token)
//            
//            self.fetchPlacesNearCoordinate(coordinate, radius: radius, types: types, pagetoken: next_page_token) { places in
//            }
//        }
        
      }
      DispatchQueue.main.async {
        
        print("final place size = \(self.placesArray.count)")
        completion(self.placesArray)
      }
    }) 
    placesTask?.resume()
  }
    
//    func fetchPlacesNearCoordinate(_ coordinate: CLLocationCoordinate2D, radius: Double, types:[String], completion: @escaping (([GooglePlace]) -> Void)) -> ()
//    {
    
    func fetchNextPage(_ coordinate: CLLocationCoordinate2D, radius: Double, types:[String], pagetoken: String) -> [GooglePlace] {
        
        var urlString = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(coordinate.latitude),\(coordinate.longitude)&radius=\(radius)"
        let typesString = types.count > 0 ? types.joined(separator: "|") : "atm"
        urlString += "&types=" + typesString
        urlString += "&key=" + googlePlacesApiKey
        urlString += "&pagetoken=" + pagetoken
        
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        if let task = placesTask , task.taskIdentifier > 0 && task.state == .running {
            task.cancel()
        }
        
        var placesArrayNextPage = [GooglePlace]()
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        placesTask = session.dataTask(with: URL(string: urlString)!, completionHandler: {data, response, error in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            if let aData = data {
                let json = JSON(data:aData, options:JSONSerialization.ReadingOptions.mutableContainers, error:nil)
                if let results = json["results"].arrayObject as? [[String : AnyObject]] {
                    for rawPlace in results {
                        let place = GooglePlace(dictionary: rawPlace, acceptedTypes: types)
                        placesArrayNextPage.append(place)
                        
                        var imgName = ""
                        
                        switch place.placeType{
                            
                        case "atm":
                            imgName = "atm"
                            
                        case "bank":
                            imgName = "bank"
                            
                        default:
                            imgName = "atm"
                        }
                        
                        place.photo = UIImage(named: imgName)
                        
                    }
                    
                    
                }
                
                if let next_page_token = json["next_page_token"].string {
                    self.pagetokenSecond = next_page_token
                    print("next_page_token = \(next_page_token)")
                }
                
            }
            DispatchQueue.main.async {
//                self.placesArray += placesArrayNextPage
                print("place size next page = \(self.placesArray.count)")
                
            }
        })
        placesTask?.resume()
        
        return placesArrayNextPage
    }
    
    func fetchPlacesNearCoordinateNextPage (_ coordinate: CLLocationCoordinate2D, radius: Double, types:[String], pagetoken: String, completion: @escaping (([GooglePlace]) -> Void)) -> () {
        var urlString = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(coordinate.latitude),\(coordinate.longitude)&radius=\(radius)"
        let typesString = types.count > 0 ? types.joined(separator: "|") : "atm"
        urlString += "&types=" + typesString
        urlString += "&key=" + googlePlacesApiKey
        urlString += "&pagetoken=" + pagetoken
        
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        if let task = placesTask , task.taskIdentifier > 0 && task.state == .running {
            task.cancel()
        }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        placesTask = session.dataTask(with: URL(string: urlString)!, completionHandler: {data, response, error in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
         //   var placesArray = [GooglePlace]()
            if let aData = data {
                let json = JSON(data:aData, options:JSONSerialization.ReadingOptions.mutableContainers, error:nil)
                if let results = json["results"].arrayObject as? [[String : AnyObject]] {
                    for rawPlace in results {
                        let place = GooglePlace(dictionary: rawPlace, acceptedTypes: types)
                        self.placesArray.append(place)
                        
                        var imgName = ""
                        
                        switch place.placeType{
                            
                        case "atm":
                            imgName = "atm"
                            
                        case "bank":
                            imgName = "bank"
                            
                        default:
                            imgName = "atm"
                        }
                        
                        place.photo = UIImage(named: imgName)
                        
                        //set real photo place
                        //            if let reference = place.photoReference {
                        //              self.fetchPhotoFromReference(reference) { image in
                        //                place.photo = image
                        //              }
                        //                print("photoReference = \(reference)")
                        //            }
                    }
                    
                    print("place size next = \(self.placesArray.count)")
                }
                
                if let next_page_token = json["next_page_token"].string {
                    self.pagetokenSecond = next_page_token
                    print("next_page_token = \(next_page_token)")
                }
                
            }
            DispatchQueue.main.async {
                completion(self.placesArray)
            }
        }) 
        placesTask?.resume()
    }

  
  func fetchPhotoFromReference(_ reference: String, completion: @escaping ((UIImage?) -> Void)) -> () {
    if let photo = photoCache[reference] as UIImage? {
      completion(photo)
    } else {
      var urlString = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=200&photoreference=\(reference)"
        urlString += "&key=\(googlePlacesApiKey)"
        
      UIApplication.shared.isNetworkActivityIndicatorVisible = true
      session.downloadTask(with: URL(string: urlString)!, completionHandler: {url, response, error in
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        if let url = url {
          let downloadedPhoto = UIImage(data: try! Data(contentsOf: url))
          self.photoCache[reference] = downloadedPhoto
          DispatchQueue.main.async {
            completion(downloadedPhoto)
          }
        }
        else {
          DispatchQueue.main.async {
            completion(nil)
          }
        }
      }) .resume()
    }
  }
}
