import UIKit
import GoogleMaps

class PlaceMarker: GMSMarker {
  let place: GooglePlace
  
  init(place: GooglePlace) {
    self.place = place
    super.init()
    
    position = place.coordinate
    
    icon = UIImage(named: checkBankNameReturnMarkerName(place.name))

    groundAnchor = CGPoint(x: 0.5, y: 1)
    appearAnimation = GMSMarkerAnimation.pop
  }
    
    func checkBankNameReturnMarkerName(_ name: String) -> String{
        var pin = ""
        switch name {
        case _ where name.lowercased().range(of: "bangkok bank") != nil, _ where name.range(of: "กรุงเทพ") != nil, _ where name.lowercased().range(of: "bbl") != nil:
            pin = "bbl"
        case _ where name.range(of: "กรุงไทย") != nil, _ where name.lowercased().range(of: "krung thai") != nil, _ where name.lowercased().range(of: "krungthai") != nil, _ where name.lowercased().range(of: "ktb") != nil:
            pin = "ktb"
        case _ where name.range(of: "กสิกร") != nil, _ where name.lowercased().range(of: "kasikorn") != nil, _ where name.lowercased().range(of: "kbank") != nil, _ where name.lowercased().range(of: "k-bank") != nil, _ where name.lowercased().range(of: "k- bank") != nil:
            pin = "kbank"
        case _ where name.range(of: "ไทยพาณิชย์") != nil, _ where name.lowercased().range(of: "scb") != nil, _ where name.lowercased().range(of: "siam commercial bank") != nil, _
        where name.lowercased().range(of: "commercial bank of thai") != nil, _
            where name.lowercased().range(of: "thailand commercial bank") != nil:
            pin = "scb"
        case _ where name.range(of: "กรุงศรี") != nil, _ where name.lowercased().range(of: "ayudhya") != nil, _ where name.lowercased().range(of: "ayudhya") != nil, _ where name.lowercased().range(of: "ayutthaya") != nil, _ where name.lowercased().range(of: "krungsri") != nil, _ where name.lowercased().range(of: "krung sri") != nil:
            pin = "krungsri"
        case _ where name.range(of: "ออมสิน") != nil, _ where name.lowercased().range(of: "government savings bank") != nil, _ where name.lowercased().range(of: "gsb") != nil:
            pin = "gsb"
        case _ where name.range(of: "เกียรตินาคิน") != nil, _ where name.lowercased().range(of: "kiatnakin") != nil:
            pin = "kiatnakin"
        case _ where name.range(of: "ธนชาต") != nil, _ where name.lowercased().range(of: "thanachart") != nil:
            pin = "thanachart"
        case _ where name.range(of: "ทหารไทย") != nil, _ where name.lowercased().range(of: "tmb") != nil, _ where name.lowercased().range(of: "thai military bank") != nil:
            pin = "tmb"
        case _ where name.range(of: "ซีไอเอ็มบี") != nil, _ where name.lowercased().range(of: "cimb") != nil, _ where name.lowercased().range(of: "thai military bank") != nil:
            pin = "cimb"
        case _ where name.range(of: "อิสลาม") != nil, _ where name.lowercased().range(of: "islamic bank") != nil, _ where name.lowercased().range(of: "ibank") != nil:
            pin = "ibank"
        case _ where name.range(of: "ไอซีบีซี") != nil, _ where name.lowercased().range(of: "commercial bank of china") != nil, _ where name.lowercased().range(of: "icbc") != nil:
            pin = "icbc"
        case _ where name.range(of: "ยูโอบี") != nil, _ where name.lowercased().range(of: "united overseas bank") != nil, _ where name.lowercased().range(of: "uob") != nil:
            pin = "uob"
        case _ where name.range(of: "ธอส") != nil, _ where name.range(of: "อาคารสงเคราะห์") != nil, _ where name.lowercased().range(of: "government housing bank") != nil, _ where name.lowercased().range(of: "ghb") != nil:
            pin = "ghb"
        case _ where name.range(of: "ทิสโก้") != nil, _ where name.lowercased().range(of: "tisco") != nil:
            pin = "tisco"
        case _ where name.range(of: "แลนด์ แอนด์ เฮ้าส์") != nil, _ where name.range(of: "land and house") != nil, _ where name.lowercased().range(of: "lh") != nil:
            pin = "lh"
        case _ where name.range(of: "เพื่อการเกษตรและสหกรณ์การเกษตร") != nil, _ where name.range(of: "ธกส") != nil, _ where name.lowercased().range(of: "agriculture") != nil, _ where name.lowercased().range(of: "baac") != nil:
            pin = "baac"
        default:
            pin = "money"
        }
        
        return pin + "_pin"
    }
}
